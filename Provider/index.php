<?php
$url = "../";
$name = "Provider";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content" >
                        <section id="provider_general" class="content" >
                            <div class='div-btn2 col-md-10 col-md-push-1 col-sm-12 col-xs-12'> 
                                <button id="create_button" class="btn btn-success pull-right"> <i class="fa fa-plus-circle fa-fw"></i> Crear provider</button>
                            </div>
                            <div class="clearfix"></div>
                            <!-- Vista editar -->
                            <section id="provider_create" class="content" hidden="">
                                <input type="hidden" value="" id="id_provider">
                                <!-- Formulario de Editar -->
                                <div class="row">
                                    <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                        <div class="box box-cdice-blue">
                                            <div class="box-header with-border">
                                                <h2 class="box-title"><i class="fa fa-plus-circle fa-fw text-success"></i> <strong>Crear un nuevo Provider</strong></h2>
                                            </div>
                                            <div class="box-body">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Nombre</label>
                                                        <input type="text" class="form-control input-new" id="nameProviderPayment" required="" autocomplete="off">
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Url</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-new" id="urlProviderPayment" required="">
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Description</label>
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-new" id="descriptionProviderPayment" required=""></textarea>
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group div-btn">
                                                    <button id="addProviderNew" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                                    <button id="provider_return" class="btn btn-primary" ><i class="fa fa-sign-out fa-fw"></i>  Regresar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                                <!-- /Formulario de Editar -->
                            </section>
                            <!-- /Vista editar-->
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-tasks fa-fw text-success"></i> <b>Listado de Providers</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <table id="provider_table" class="table table-striped"></table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>
                    </section>
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/tableExport.js"></script>
        <script src="../lib/bootstrap-table/extensions/multiple-sort/bootstrap-table-multiple-sort.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="../lib/common/js/provider.js"></script>
        <script>
       $(document).ready(function () {
           ListProvider.init();
           CreateProvider.init();

           HideLoader();
       });
        </script>
    </body>
</html>
