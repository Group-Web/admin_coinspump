<?php
$url = "../";
$name = "Login";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/common/css/login.css">
    </head>
    <body class="hold-transition ">
        <div class="container">
            <div class="login-container">
                <?php loader(); ?>
                <section class="content" >
                    <div id="output"></div>
                    <div class="avatar"> 
                        <img src="../lib/common/img/isotipo.png" width="50px"/>
                    </div>
                    <h4>Panel Administrativo</h4>
                    <div class="form-box">
                        <div id="login-form">
                            <input id="loginUser" type="text" placeholder="username">
                            <input id="passwordUser" type="password" placeholder="password">
                            <input id="login" class="btn btn-cdice-blue btn-block login" value="Enviar" type="button">
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/common/js/login.js"></script>
    </body>
</html>
