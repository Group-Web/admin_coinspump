<?php
$url = "../";
$name = "Cryptocurrency";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content" >

                        <section id="crypto_general" class="content" >
                            <div class='div-btn2 col-md-10 col-md-push-1 col-sm-12 col-xs-12'> 
                                <button id="create_button" class="btn btn-success pull-right"> <i class="fa fa-plus-circle fa-fw"></i> Crear Cryptocurrency</button>
                            </div>
                            <div class="clearfix"></div>
                            <!-- Vista Crear -->
                            <section id="crypto_create" class="content" hidden="">
                                <!-- Formulario de Editar -->
                                <div class="row">
                                    <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                        <div class="box box-success">
                                            <div class="box-header with-border">
                                                <h2 class="box-title"><i class="fa fa-pencil fa-fw text-success"></i> <b>Crear Cryptocurrency</b></h2>
                                            </div>
                                            <div class="box-body">
                                                <div class="form-group col-md-6">
                                                    <label>Name</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-codepen fa-fw"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="nameCryptocurrency " id="nameCryptocurrency">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Code</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-codepen fa-fw"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="codeCryptocurrency" id="codeCryptocurrency">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Web URL</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-file-image-o fa-fw"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="webCryptocurrency " id="webCryptocurrency">
                                                    </div>
                                                </div> 
                                                <div class="form-group col-md-6">
                                                    <label>Imagen URL</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-file-image-o fa-fw"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="imageCryptocurrency " id="imageCryptocurrency">
                                                    </div>
                                                </div> 
                                                <div class="form-group col-md-12 div-btn">
                                                    <button id="btn-create-crypto" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Crear</button>
                                                    <button class="btn btn-primary" id="btn-regresar"><i class="fa fa-sign-out fa-fw"></i>  Regresar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                                <!-- /Formulario de Editar -->
                            </section>
                            <!-- /Vista Crear -->

                            
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-tasks fa-fw text-success"></i> <b>Listado total de Cryptocurrencies</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <table id="crypto_table" class="table table-striped"></table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>

                        
                    </section>
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/tableExport.js"></script>
        <script src="../lib/bootstrap-table/extensions/multiple-sort/bootstrap-table-multiple-sort.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="../lib/common/js/crypto.js"></script>
        <script>
       $(document).ready(function () {
           ListCrypto.init();
           HideLoader();
       });
        </script>
    </body>
</html>
