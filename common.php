<?php
$web_name = "Administrador";
$web_url_desarrollador = "";
$web_name_desarrollador = "";
$version = "1.0.0";

// cabecera y metas 
function head($base_url, $who_is) {
   global $web_name;
   ?>
   <meta charset="utf-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
   <meta http-equiv="expires" content="0">
   <meta http-equiv="Cache-Control" content="no-cache">
   <meta http-equiv="Pragma" CONTENT="no-cache">

   <title><?php echo $web_name . " | " . $who_is; ?> </title>
   <script type="text/javascript">
   <?php if ($who_is == "Login") { ?>
         if (localStorage['coin_admin_token']) {
             window.location.href = "<?php echo $base_url; ?>";
             $(document).ready(function () {
                 UserMenu('<?php echo $base_url; ?>');
             });
         }
   <?php } else { ?>
         if (!localStorage['coin_admin_token']) {
             window.location.href = "<?php echo $base_url; ?>Login";
         }
   <?php } ?>
   </script>
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/bootstrap/css/bootstrap.min.css"/>
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/font-awesome-4.7.0/css/font-awesome.min.css" >
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/select2/select2.min.css" >
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/common/css/AdminLTE.min.css">
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/common/css/skins/_all-skins.min.css">
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/common/css/custom.css"/>
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/SweetAlert/sweetalert.css" >
    <link rel="stylesheet" href="<?php echo $base_url; ?>lib/datepicker/datepicker3.css"/>
   <link rel="shortcut icon" type="image/x-icon" href="<?php echo $base_url; ?>/lib/common/img/isotipo.png"/>
   <?php
}

//Menu
function menu($base_url, $who_is) {
   global $web_name;
   ?>
   <!-- TOP MENU -->
   <header class="main-header">
       <a href="<?php echo $base_url; ?>" class="logo visible-lg visible-md visible-sm">
           <span class="logo-mini" style="color:white"><i class="fa fa-gears"></i></span>
           <span class="logo-lg" style="color:white"><i class="fa fa-cog "></i> <?php echo $web_name; ?></span>
       </a>
       <nav class="navbar navbar-static-top">
           <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
               <span class="sr-only">Toggle navigation</span>
           </a>
           <div id="menu-top" class="pull-left notifications-menu visible-xs">
               <span class="logo-lg" style="color:white"> <?php echo $web_name; ?></span>
           </div>
           <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                   <li>
                       <a onclick="logout('<?php echo $base_url; ?>');" data-toggle="control-sidebar"><i class="fa fa-sign-out"></i></a>
                   </li>
               </ul>
           </div>
       </nav>
   </header>
   <!-- /TOP MENU -->

   <!-- LEFT MENU -->
   <aside class="main-sidebar">
       <section class="sidebar">
           <div class="user-panel">
               <div class="pull-left image"><img id="img-dash" src="<?php echo $base_url; ?>lib/common/img/admin_user.png" class="img-circle" alt="User Image"></div>
               <div class="pull-left info">
                   <p id="name_user_dash">User</p>
                   <a class="cerrar-session-btn" onclick="logout('<?php echo $base_url; ?>');"><i class="fa fa-circle text-red"></i> Cerrar Sesión</a>
               </div>
           </div>
           <ul id='module-admin' class="sidebar-menu">
               <!-- MODULO DE ADMINISTRADOR -->
               <li class="header"><b>MENÚ ADMINISTRADOR</b></li>

               <!-- PANEL DE CONTROL -->
               <li <?php if ($who_is == 'Dashboard') { ?>class="active"<?php } ?>>
                   <a href="<?php echo $base_url; ?>">
                       <i class="fa fa-dashboard"></i> <span>Dashboard</span><span class="pull-right-container"></span>
                   </a>
               </li>
               <!-- /PANEL DE CONTROL -->

               <!-- Chat -->
               <li class="treeview <?php if (substr($who_is, 0, 4) == 'Chat') { ?>active<?php } ?>">
                   <a href="#">
                       <i class="fa fa-codepen fa-fw"></i><span>&nbsp; Chat</span>
                       <span class="pull-right-container">
                           <i class="fa fa-angle-down pull-right"></i>
                       </span>
                   </a>
                   <ul class="treeview-menu">
                       <li class="<?php if ($who_is == 'ChatNew') { ?>active<?php } ?>">
                           <a href="<?php echo $base_url; ?>Chat/chat_new.php">
                               <i class="fa fa-plus-circle fa-fw"></i>&nbsp; Crear
                           </a>
                       </li>
                       <li class="<?php if ($who_is == 'Chat') { ?>active<?php } ?>">
                           <a href="<?php echo $base_url; ?>Chat">
                               <i class="fa fa-list fa-fw"></i>&nbsp; Listado
                           </a>
                       </li>
                   </ul>
               </li>  
               <!-- /Chat -->

               <!-- Payment -->
               <li class=" <?php if (substr($who_is, 0, 7) == 'Payment') { ?>active<?php } ?>">
                   <a href="<?php echo $base_url; ?>Payment/">
                       <i class="fa fa-money fa-fw"></i><span>&nbsp; Payments</span>
                   </a>
               </li>  
               <!-- /Chat -->
               
               <!-- Service -->
               <li class=" <?php if (substr($who_is, 0, 7) == 'Service') { ?>active<?php } ?>">
                   <a href="<?php echo $base_url; ?>Service/">
                       <i class="fa fa-share-square fa-fw"></i><span>&nbsp; Service</span>
                   </a>
               </li> 
               <!-- /Service -->

               <!-- Provider -->
               <li class=" <?php if (substr($who_is, 0, 7) == 'Provider') { ?>active<?php } ?>">
                   <a href="<?php echo $base_url; ?>Provider/">
                       <i class="fa fa-product-hunt fa-fw"></i><span>&nbsp; Provider</span>
                   </a>
               </li> 
               <!-- /Provider -->

               <!-- Wallets -->
               <li class=" <?php if (substr($who_is, 0, 14) == 'Wallet') { ?>active<?php } ?>">
                   <a href="<?php echo $base_url; ?>Wallet/">
                       <i class="fa fa-shopping-bag fa-fw"></i><span>&nbsp; Wallet</span>
                   </a>
               </li> 
               <!-- /Wallets -->

               <!-- Cryptocurrencies -->
               <li class=" <?php if (substr($who_is, 0, 14) == 'Cryptocurrency') { ?>active<?php } ?>">
                   <a href="<?php echo $base_url; ?>Cryptocurrency/">
                       <i class="fa fa-bitcoin fa-fw"></i><span>&nbsp; Cryptocurrency</span>
                   </a>
               </li> 
               <!-- /Cryptocurrencies -->

               <!-- USUARIOS -->
               <li class="treeview <?php if (substr($who_is, 0, 4) == 'User') { ?>active<?php } ?>">
                   <a href="#">
                       <i class="fa fa-users fa-fw"></i><span>&nbsp; Users</span>
                       <span class="pull-right-container">
                           <i class="fa fa-angle-down pull-right"></i>
                       </span>
                   </a>
                   <ul class="treeview-menu">
                       <!--<li class="<?php if ($who_is == 'UserNew') { ?>active<?php } ?>">
                           <a href="<?php echo $base_url; ?>User/user_new.php">
                               <i class="fa fa-user-plus fa-fw"></i>&nbsp; Crear
                           </a>
                       </li>-->
                       <li class="<?php if ($who_is == 'User') { ?>active<?php } ?>">
                           <a href="<?php echo $base_url; ?>User">
                               <i class="fa fa-user fa-fw"></i>&nbsp; List
                           </a>
                       </li>
                   </ul>
               </li>  
               <!-- /USUARIOS -->

               <!-- MODULO COMUN -->    
               <li class="header"><b>PERFIL DE USUARIO</b></li>

               <li <?php if ($who_is == 'EditProfile') { ?>class="active"<?php } ?>>
                   <a href="<?php echo $base_url; ?>User/edit_user.php">
                       <i class="fa fa-cog fa-fw"></i><span>&nbsp; Editar mi perfil</span>
                   </a>
               </li>
               <!-- /MODULO COMUN -->    

           </ul>
           <!-- /MODULO DE ADMINISTRADOR -->

       </section>
   </aside>
   <!-- /LEFT MENU -->

   <?php
}

//Pie de pagina
function footer() {
   global $web_name, $web_url_desarrollador, $web_name_desarrollador, $version;
   ?>
   <footer class="main-footer">
       <div class="pull-right hidden-xs"><b>Version</b> <?php echo $version; ?> - Desarrollado por <a href="<?php echo $web_url_desarrollador; ?>"><?php echo $web_name_desarrollador; ?></a></div>
       <strong> Copyright &copy; <?php echo $web_name; ?> 2017 - All rights reserved. </strong> 
   </footer>
   <?php
}

//Cargando
function loader() {
   ?>
   <!--Logo cargando-->
   <div id = "cargador" hidden="">
       <div id="loader-wrapper">
           <div id="loader"></div>
           <h4>Cargando...</h4>
       </div>
   </div>
   <!--/Logo Cargando-->
   <?php
}

//javascripts comunes
function scripts($base_url) {
   ?>
   <script src="<?php echo $base_url; ?>lib/jQuery/jquery-2.2.3.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/bootstrap/js/bootstrap.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/select2/select2.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/common/js/BaseUrl.js"></script>
   <script src="<?php echo $base_url; ?>lib/common/js/app.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/common/js/demo.js"></script>
   <script src="<?php echo $base_url; ?>lib/common/js/custom.js"></script>
   <script src="<?php echo $base_url; ?>lib/SweetAlert/sweetalert.js"></script>
   <script src="<?php echo $base_url; ?>lib/jwt/jwt-decode.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/datepicker/bootstrap-datepicker.js"></script>
   <?php
}
?>
