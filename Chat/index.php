<?php
$url = "../";
$name = "Chat";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content" >
                        <section id="brands_general" class="content" >
                            <div class='div-btn2 col-md-10 col-md-push-1 col-sm-12 col-xs-12'> 
                                <a href="chat_new.php" class="btn btn-success pull-right"> <i class="fa fa-plus-circle fa-fw"></i> Crear chat</a>
                            </div>
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-tasks fa-fw text-success"></i> <b>Listado total de chats</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <table id="chat_table" class="table table-striped"></table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>

                        <!-- Vista editar -->
                        <section id="brands_detail" class="content" hidden="">
                            <input type="hidden" value="" id="id_brands">
                            <!-- Formulario de Editar -->
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-pencil fa-fw text-success"></i> <b>Editar marca</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group col-md-12">
                                                <label>Nombre</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <input type="text" class="form-control" name="name_brand " id="name_brand">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Descripción</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-commenting fa-fw"></i>
                                                    </div>
                                                    <textarea class="form-control" rows="3" id="description_brand" maxlength="250"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Imagen</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-file-image-o fa-fw"></i>
                                                    </div>
                                                    <input type="file" id="file-img" class="form-control">
                                                </div>
                                            </div> 
                                            <div class="form-group col-md-6">
                                                <label>Estatus</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-toggle-on fa-fw"></i>
                                                    </div>
                                                    <select id="combo-status" class="form-control combo-status"></select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <div class="checkbox">
                                                        <label><input id="priority_brands_prince" type="checkbox" value="">Mostrar en la lista de No OSS</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 div-btn">
                                                <button id="btn-update-brand" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Actualizar</button>
                                                <button class="btn btn-primary" id="btn-regresar"><i class="fa fa-sign-out fa-fw"></i>  Regresar</button>
                                                <a href="" class="btn btn-primary"> <i class="fa fa-plus-circle fa-fw"></i> Crear chat</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                            <!-- /Formulario de Editar -->
                        </section>
                        <!-- /Vista editar-->
                    </section>
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/tableExport.js"></script>
        <script src="../lib/bootstrap-table/extensions/multiple-sort/bootstrap-table-multiple-sort.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="../lib/common/js/chat.js"></script>
        <script>
       $(document).ready(function () {
           ListChat.init();
           HideLoader();
       });
        </script>
    </body>
</html>
