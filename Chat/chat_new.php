<?php
$url = "../";
$name = "ChatNew";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
        <style>
            #add_payment_type{
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!-- Vista Detallada del crear brand-->
                <section id="content-principal" hidden="" >
                    <section class="content">
                        <div class="row">
                            <div class="col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                                <div class="box box-cdice-blue">
                                    <div class="box-header with-border">
                                        <h2 class="box-title"><i class="fa fa-plus-circle fa-fw text-success"></i> <strong>Crear un nuevo chat</strong></h2>
                                    </div>
                                    <div class="box-body">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>nameChat</label>
                                                <input type="text" class="form-control input-new" id="nameChat" placeholder="nameChat" required="" autocomplete="off">
                                                <div class="div-validador">
                                                    <div>&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>startDateChat</label>
                                                <input type="datetime-local" class="form-control input-new" id="startDateChat" placeholder="startDateChat" required="" autocomplete="off">
                                                <div class="div-validador">
                                                    <div>&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>endDateChat</label>
                                                <input type="datetime-local" class="form-control input-new" id="endDateChat" placeholder="endDateChat" required="" autocomplete="off">
                                                <div class="div-validador">
                                                    <div>&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>cryptocurrency</label>
                                                <select class="form-control input-new-c cryptocurrency" id="cryptocurrency" ></select>
                                                <div class="div-validador">
                                                    <div>&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>TypeChat</label>
                                                <select class="form-control input-new-c" id="typeChat" ></select>
                                                <div class="div-validador">
                                                    <div>&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>descriptionChat</label>
                                            <div class="form-group">
                                                <textarea type="text" class="form-control input-new" id="descriptionChat" required=""></textarea>
                                                <div class="div-validador">
                                                    <div>&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-center">
                                            <button id="add_payment_type" type="submit" class="btn btn-reddit">
                                                <i class="fa fa-plus fa-fw"></i> &nbsp; Add metodo pago
                                            </button>
                                            <table id="priceChatTable" class="table table-striped" ></table>
                                            <div id="priceChatTableLegend"></div>
                                        </div>

                                        <div class="form-group div-btn">
                                            <button id="addChatNew" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                            <a href="../Chat/" class="btn btn-primary" ><i class="fa fa-sign-out fa-fw"></i>  Regresar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </section>
                    <!-- /Vista Detallada del Usuario-->
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/common/js/validate.js"></script>
        <script src="../lib/common/js/chat.js"></script>
        <script>
       $(document).ready(function () {
           CreateChat.init();
           HideLoader();
       });
        </script>
    </body>
</html>
