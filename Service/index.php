<?php
$url = "../";
$name = "Service";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content" >
                        <section id="service_general" class="content" >
                            <!--<div class='div-btn2 col-md-10 col-md-push-1 col-sm-12 col-xs-12'> 
                                <button id="create_button" class="btn btn-success pull-right"> <i class="fa fa-plus-circle fa-fw"></i> Crear service</button>
                            </div>-->
                            <div class="clearfix"></div>
                            <!-- Vista editar -->
                            <section id="service_create" class="content" hidden="">
                                <input type="hidden" value="" id="idServicePayment">
                                <!-- Formulario de Editar -->
                                <div class="row">
                                    <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                        <div class="box box-cdice-blue">
                                            <div class="box-header with-border">
                                                <h2 class="box-title"><i class="fa fa-plus-circle fa-fw text-success"></i> <strong>Crear un nuevo Service</strong></h2>
                                            </div>
                                            <div class="box-body">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Nombre</label>
                                                        <input type="text" class="form-control input-new" id="nameServicePayment" required="" autocomplete="off">
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Description</label>
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-new" id="descriptionServicePayment" required=""></textarea>
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                
                                                <div class="col-md-12 text-center">
                                                    <button id="add_service_payment_price" class="btn btn-reddit">
                                                        <i class="fa fa-plus fa-fw"></i> &nbsp; Add metodo pago
                                                    </button>
                                                    <table id="priceServiceTable" class="table table-striped" ></table>
                                                    <div id="priceServiceTableLegend"></div>
                                                </div>

                                                <div class="form-group div-btn">
                                                    <button id="addServiceNew" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                                    <button id="service_return" class="btn btn-primary" ><i class="fa fa-sign-out fa-fw"></i>  Regresar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                                <!-- /Formulario de Editar -->
                            </section>
                            <!-- /Vista editar-->
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-tasks fa-fw text-success"></i> <b>Listado de Services</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <table id="service_table" class="table table-striped"></table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>
                    </section>
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/tableExport.js"></script>
        <script src="../lib/bootstrap-table/extensions/multiple-sort/bootstrap-table-multiple-sort.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="../lib/common/js/service.js"></script>
        <script>
       $(document).ready(function () {
           ListService.init();
           CreateService.init();

           HideLoader();
       });
        </script>
    </body>
</html>
