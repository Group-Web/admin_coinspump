<?php
$url = "./";
$name = "Dashboard";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="lib/common/css/dashboard.css" >
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu('<?php echo $url; ?>');">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content">
                        <div>
                            <!-- Indicadores-->
                            <div id="home_class_section_1" class="row">
                                <div class="col-lg-3 col-xs-6">
                                    <div class="small-box bg-primary">
                                        <div class="inner">
                                            <h3 id="home_ind_user"></h3>
                                            <p><b>Usuarios Activos</b></p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <a href="User/" class="small-box-footer"><b>Mas información</b> <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="small-box bg-primary">
                                        <div class="inner">
                                            <h3 id="home_ind_total_phrase"></h3>
                                            <p><b>Por validar Email</b></p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <a href="Chat/" class="small-box-footer"><b>Mas información</b> <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <div class="small-box bg-primary">
                                        <div class="inner">
                                            <h3 id="home_ind_approve_phrase"></h3>
                                            <p><b>Pagos Por Validar</b></p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                        <a href="Payment/" class="small-box-footer"><b>Mas información</b> <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xs-6">
                                    <div class="small-box bg-primary">
                                        <div class="inner">
                                            <h3 id="home_ind_complain_phrase"></h3>
                                            <p><b>Premios canjeados</b></p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-cart-plus"></i>
                                        </div>
                                        <a href="Product/" class="small-box-footer"><b>Mas información</b> <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /Indicadores-->

                            <!-- Graficos--> 
                            <div id="home_class_section_2" class="row">
                                <div class="col-md-12">
                                    <div class="box box-cdice-purple">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"> <b>usuarios por pais</b></h3>
                                        </div>
                                        <div class="box-body">
                                            <div class="chart">
                                                <div id="home_chart_bar" style="min-width: 310px;max-width: 600px; margin: 0 auto"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--                            <div class="col-md-6">
                                                                <div class="box box-success">
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title">Usuario por pais</h3>
                                                                    </div>
                                                                    <div class="box-body">
                                                                        <div class="chart">
                                                                            <div id="home_chart_bar_country_user" style="min-width: 310px;max-width: 600px; margin: 0 auto"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                            </div>
                            <!-- /Graficos--> 

                        </div>
                    </section>

                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="lib/highcharts/highcharts.js"></script>
        <script src="lib/highcharts/highcharts-3d.js"></script>
        <!--<script src="lib/highcharts/highcharts_export.js"></script>-->
        <script src="lib/common/js/dashboard.js"></script>
        <script>
       $(document).ready(function () {
           LoadIndex();
           HideLoader();
       });
        </script>
    </body>
</html>
