var UrlCryptocurrency = BaseUrl + 'cryptocurrency/';
var UrlProvider = BaseUrl + 'cryptocurrency/provider/';
var UrlTypePayment = BaseUrl + 'payment/type';
var UrlTypeChat = BaseUrl + 'default/type_chat';

// Mensajes de error en los servicios
function ErrorMessage(xhr, url) {
    var text = "";
    if (xhr.status == 0 || xhr.status == 500) {
        text = "Los servicios no se encuentran disponibles, intente nuevamente en unos minutos. (" + xhr.status + ")";
    } else if (JSON.parse(xhr.responseText).code == 'TKN') {
        localStorage.clear();
        swal({
            title: "<span class='text-danger'> ¡Sesión caducada! </span>",
            text: JSON.parse(xhr.responseText).message,
            imageUrl: '../lib/common/img/coins2pump-logo.png',
            imageSize: '150x80',
            imageAlt: 'Custom image',
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Salir",
            closeOnConfirm: false,
            allowOutsideClick: false,
            html: true
        }, function () {
            window.location.href = "./";
        });
    } else {
        url = "no";
        text = JSON.parse(xhr.responseText).message + " (" + JSON.parse(xhr.responseText).code + ")";
    }
    if (text != "") {
        GeneratePop("¡Se ha generado un error!", text, "error", url);
    }
}
function GeneratePop(title, text, type, url) {
    swal({
        title: title,
        text: text,
        type: type,
        closeOnConfirm: true,
        allowOutsideClick: true,
        html: true
    }, function (isConfirm) {
        if (url != "no") {
            window.location.href = url;
        }
    });
}

// LOGIN
function logout(url) {
    swal({
        title: "¡Cerrar Sesión!",
        text: '¿Desea salir del Administrador?',
        imageUrl: url + 'lib/common/img/coins2pump-logo.png',
        imageSize: '150x80',
        imageAlt: 'Custom image',
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        cancelButtonClass: "btn-danger",
        closeOnConfirm: false,
        allowOutsideClick: true
    },
            function () {
                localStorage.clear();
                window.location.href = url + "Login";
            });

}
function UserMenu(url) {
    setTimeout(function () {
        $("#name_user_dash").html('<p>' + localStorage['coin_admin_login'] + '</p>');
    }, 100);
    ShowLoader();
}

// Loader
function ShowLoader(valida = null) {
//    if (valida === true) {
//        var decoded = jwt_decode(localStorage['coin_admin_token']);
//        ValidToken(decoded);
//    }
    $("#content-principal").show();
    $("#cargador").show();
}
function HideLoader() {
    setTimeout(function () {
        $("#cargador").fadeOut(500, function () {
//            $("#content-principal").fadeIn(1000);
        });
    }, 1000);
}

// Validar Token 
function ValidToken(decoded) {
    var myDate = new Date(1000 * decoded.exp);
    var today = new Date();
    if ((myDate - today).toLocaleString() < 0) {
        localStorage.clear();
        swal({
            title: "<span class='text-danger'> ¡Sesión caducada! </span>",
            text: JSON.parse(xhr.responseText).message,
            imageUrl: '../lib/common/img/logo-coins2pump.png',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Salir",
            closeOnConfirm: false,
            allowOutsideClick: false,
            html: true
        }, function () {
            window.location.href = "./";
        });
    }
}

// Funciones
function nFormatter(num, digits) {
    if (parseInt(num) < 9999) {
        return num;
    }
    var si = [
        {value: 1, symbol: ""},
        {value: 1E3, symbol: "k"},
        {value: 1E6, symbol: "M"},
        {value: 1E9, symbol: "G"},
        {value: 1E12, symbol: "T"},
        {value: 1E15, symbol: "P"},
        {value: 1E18, symbol: "E"}
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}
function GetCryptocurrency(etiqueta) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {"Authorization": localStorage['coins_token']},
        type: "GET",
        url: UrlCryptocurrency,
        beforeSend: function (xhr) {
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona una cryptocurrency</option>');
            $.each(result.data, function (i, item) {
                $(etiqueta).append("<option value = '" + item.idCryptocurrency + "'>" + item.nameCryptocurrency + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
function GetWalletProvider(etiqueta) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {"Authorization": localStorage['coins_token']},
        type: "GET",
        url: UrlProvider,
        beforeSend: function (xhr) {
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un Provider</option>');
            $.each(result.data, function (i, item) {
                $(etiqueta).append("<option value = '" + item.idProviderPayment + "'>" + item.nameProviderPayment + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
function GetTypePayment(etiqueta) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {"Authorization": localStorage['coins_token']},
        type: "GET",
        url: UrlTypePayment,
        beforeSend: function (xhr) {
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona una cryptocurrency</option>');
            $.each(result.data, function (i, item) {
                $(etiqueta).append("<option value = '" + item.idCryptocurrency + "'>" + item.nameCryptocurrency + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
function GetTypeChat(etiqueta) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {"Authorization": localStorage['coins_token']},
        type: "GET",
        url: UrlTypeChat,
        beforeSend: function (xhr) {
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un tipo de chat</option>');
            $.each(result.data, function (i, item) {
                $(etiqueta).append("<option value = '" + item.idTypeChat + "'>" + item.nameTypeChat + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            ErrorMessage(xhr, "no");
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}

// Cargar ComboBox
function GetCountries(etiqueta,selected) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {},
        type: "GET",
        url: UrlServiceCountry,
        beforeSend: function (xhr) {
            $(etiqueta).prop('disabled', true);
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un País</option>');
            $.each(result.data, function (i, item) {
                var selectedTag = "";
                if (parseInt(item.idCountry) == parseInt(selected)) {
                    selectedTag = "selected";
                }
                $(etiqueta).append("<option "+selectedTag+" value = '" + item.idCountry + "'>" + item.nameCountry + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
//            GeneratePopUpAmigo("error", "Error cargando los paises");
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
// Cargar ComboBox
function GetGender(etiqueta,selected) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {},
        type: "GET",
        url: UrlServiceGender,
        beforeSend: function (xhr) {
            $(etiqueta).prop('disabled', true);            
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un Genero</option>');
            $.each(result.data, function (i, item) {
                var selectedTag = "";
                if (parseInt(item.idGender) == parseInt(selected)) {
                    selectedTag = "selected";
                }
                $(etiqueta).append("<option "+selectedTag+" value = '" + item.idGender + "'>" + item.nameGender + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });


}
function GetRole(etiqueta,selected) {

    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {},
        type: "GET",
        url: UrlServiceRole,
        beforeSend: function (xhr) {
            $(etiqueta).prop('disabled', true);            
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un Rol</option>');
            $.each(result.data, function (i, item) {
                var selectedTag = "";
                if (parseInt(item.idRole) == parseInt(selected)) {
                    selectedTag = "selected";
                }
                $(etiqueta).append("<option "+selectedTag+" value = '" + item.idRole + "'>" + item.nameRole + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });

}