var urlListChatAdmin = BaseUrl + 'chat/list/admin/';
var urlListChat = BaseUrl + 'chat/list';
var urlSaveChat = BaseUrl + 'chat/';

// Objetos
var ListChat = {
    init: function () {
        // Preloader
        sessionStorage.removeItem('priceChat');
        localStorage.removeItem('idChat');
        this.listChats();
        $('#listChats').on('click', '.payment_chat', function (e) {
            localStorage["idChat"] = $(this).children('.payment_chat').val();
            localStorage["idService"] = 1;
            window.location.href = "../Dashboard/Payment/";
        });
    },
    listChats: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: urlListChatAdmin,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                $("#chat_table").bootstrapTable("destroy");
                $('#chat_table').bootstrapTable({
                    columns: [
                        {field: 'nameChat', title: 'Nombre'},
                        {field: 'nameCryptocurrency', title: 'Cryptocurrency'},
                        {field: 'startChat', title: 'Fecha inicio'},
                        {field: 'endChat', title: 'Fecha fin'},
                        {field: 'nameStatusChat', title: 'Estatus'},
                        {
                            field: 'idChat',
                            title: 'Editar',
                            align: 'center',
                            formatter: function (value) {
                                    return '<a class="like" ><input class="like2" value="' + parseInt(value) + '" hidden/><i class="text-primary fa fa-pencil fa-fw"></i></a>';
                            }
                        }
//                        {
//                            field: 'idStatusChat',
//                            title: 'Action',
//                            align: 'center',
//                            formatter: function (value, row) {
//                                switch (value) {
//                                    case 1:
//                                        return '<a class="payment_chat" data-toggle="tooltip" title="Pagamento">' +
//                                                '<input class="payment_chat" value="' + row.idChat + '" hidden/>' +
//                                                '<i class="text-primary far fa-credit-card"></i></a>';
//                                        break;
//                                    case 2:
//                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
//                                                '<i class="text-warning far fa-edit"></i></a>';
//                                        break;
//                                    case 3:
//                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
//                                                '<i class="text-success far fa-check-square"></i></a>';
//                                        break;
//                                    case 4:
//                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
//                                                '<i class="text-primary fas fa-calendar-check"></i></a>';
//                                        break;
//                                    case 5:
//                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
//                                                '<i class="text-danger far fa-window-close"></i></a>';
//                                        break;
//                                    default:
//                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
//                                                '<i class="text-primary fas fa-exclamation-circle"></i></a>';
//                                        break;
//                                }
//                            }
//                        }
                    ],
                    data: result.data,
                    pagination: false,
                    search: false,
                    showToggle: false
                });
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};
var CreateChat = {
    init: function () {
        var that = this;
        sessionStorage.removeItem('priceChat');
        GetCryptocurrency("#cryptocurrency");
        GetTypeChat("#typeChat");
        // Eventos
        $('#typeChat').on('change', function (e) {
            that.paintPayment();
            $("#add_payment_type").show();
            $("#priceChatTable").show();
            $("#priceChatTableLegend").show();
            if ($(this).val() == 2) {
                $("#priceChatTable").bootstrapTable("destroy");
                $("#add_payment_type").hide();
                $("#priceChatTableLegend").hide();
            }
        });
        $('#createNewChat').on('click', function (e) {
            CreateChat.createNewChat();
        });
        $('#priceChatTable').on('click', '.delete', function (e) {
            CreateChat.deletePayment($(this).children('.delete').val());
        });
        $("#add_payment_type").on("click", function () {
            CreateChat.addModalPayment();
        });
        $("#addChatNew").on("click", function () {
            CreateChat.saveNewChat();
        });
    },
    createNewChat: function () {
        GetCryptocurrency("#cryptocurrency");
        $("#div-list").hide();
        $(".div-create").show();
    },
    addModalPayment: function () {
        var that = this;
        GetTypePayment("#cryptocurrency2");
        swal({
            title: "<h4>¡Agrega un nuevo metodo de pago!</h4>",
            text: this.structureModalPayment(),
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Guardar",
            cancelButtonText: "Cancelar",
            cancelButtonClass: "btn-default",
            allowOutsideClick: true,
            html: true,
            closeOnConfirm: true
        }, function () {
            that.addPayment($("#valuePriceChat").val(), $("#descriptionPriceChat").val(), $("#cryptocurrency2 option:selected").val(), $("#cryptocurrency2 option:selected").text());
            that.paintPayment();
        });
    },
    structureModalPayment: function () {
        return '</br><div class="col-md-12 input-group">' +
                '<input placeholder="valuePriceChat" type="text" class="form-control" id="valuePriceChat">' +
                '</div></br><div class="col-md-12 input-group">' +
                '<input placeholder="descriptionP" type="text" class="form-control" id="descriptionPriceChat">' +
                '</div></br><div class="col-md-12 input-group">' +
                '<select class="form-control input-new-c cryptocurrency" id="cryptocurrency2" ></select>' +
                '</div></br>';
    },
    addPayment: function (valuePriceChat, descriptionPriceChat, cryptocurrency, nameCryptocurrency) {
        if (sessionStorage['priceChat']) {
            var array = JSON.parse(sessionStorage['priceChat']);
            var data = array.data;
            var msj = "";
            for (var i in data) {
                if (cryptocurrency == data[i].cryptocurrency) {
                    msj = '<small>- Ya existe un monto con esta criptomoneda</small>';
                }
                if (msj != '') {
                    $(".rs").html(msj);
                    return false;
                }
            }
            array.data.push({
                valuePriceChat: valuePriceChat,
                descriptionPriceChat: descriptionPriceChat,
                cryptocurrency: cryptocurrency,
                nameCryptocurrency: nameCryptocurrency
            });
            sessionStorage['priceChat'] = JSON.stringify({data: array.data});
        } else {
            sessionStorage['priceChat'] = JSON.stringify({data: [{valuePriceChat: valuePriceChat, descriptionPriceChat: descriptionPriceChat, cryptocurrency: cryptocurrency, nameCryptocurrency: nameCryptocurrency}]});
        }
    },
    paintPayment: function () {
        if (sessionStorage['priceChat']) {
            var data = JSON.parse(sessionStorage['priceChat']).data;
            $("#priceChatTable").show();
            $("#priceChatTableLegend").hide();
            $("#priceChatTable").bootstrapTable("destroy");
            $('#priceChatTable').bootstrapTable({
                columns: [
                    {field: 'nameCryptocurrency', title: 'cryptocurrency'},
                    {field: 'valuePriceChat', title: 'Monto'},
                    {field: 'descriptionPriceChat', title: 'Descripcion'},
                    {
                        field: 'cryptocurrency',
                        title: 'Action',
                        align: 'center',
                        formatter: function (value, row) {
                            return '<a class="delete" data-toggle="tooltip" title="Delete">' +
                                    '<input class="delete" value="' + value + '" hidden/>' +
                                    '<i class="text-danger fa fa-window-close"></i></a>';
                        }
                    }
                ],
                data: data,
                pagination: true,
                search: false,
                showToggle: false
            });
        } else {
            $("#priceChatTable").bootstrapTable("destroy");
            $("#priceChatTable").hide();
            $("#priceChatTableLegend").show();
            $("#priceChatTableLegend").html("<p>Para agregar un precio al chat presione el boton +, de lo contrario sera tomado como un chat gratuito</p>");
        }

    },
    deletePayment: function (cryptocurrency) {
        if (sessionStorage['priceChat']) {
            var data = JSON.parse(sessionStorage['priceChat']).data;
            for (var i in data) {
                if (cryptocurrency == data[i].cryptocurrency) {
                    data.splice(i, 1);
                }
            }
            if (data != '') {
                sessionStorage['priceChat'] = JSON.stringify({data: data});
            } else {
                sessionStorage.removeItem('priceChat');
            }
            this.paintPayment();
        }
    },
    saveNewChat: function () {
        var myData = {
            nameChat: $("#nameChat").val(),
            startDateChat: $("#startDateChat").val(),
            endDateChat: $("#endDateChat").val(),
            descriptionChat: $("#descriptionChat").val(),
            cryptocurrency: $("#cryptocurrency").val(),
            typeChat: $("#typeChat").val()
        };
        if (sessionStorage['priceChat'] && parseInt(myData["typeChat"]) != 1) {
            var price = JSON.parse(sessionStorage['priceChat']);
            myData['priceChat'] = price.data;
            console.log(myData);
        }

        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify(myData),
            url: urlSaveChat,
            beforeSend: function (xhr) {},
            success: function (result) {
                console.log(result);
                GeneratePop(result.message , "", "success" , "");
                //alert("chat creado");
                //window.location.href = "";
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
//                alert("error creando chat");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};