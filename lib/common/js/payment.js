var urlListPayment = BaseUrl + 'payment/list';
var urlValidatetPayment = BaseUrl + 'payment/admin_validate';

// Objetos
var ListPayment = {
    init: function () {
        var that = this;
        // Preloader
        HideLoader();
        that.listChats();

//        $('#payment_table').on('click', '.edit_payment', function (e) {
//            CreateChat.deletePayment($(this).children('.delete').val());
//        });
        $('#payment_table').on('click', '.valid_payment', function (e) {
            that.validatePayment($(this).children('.valid_payment').val());
        });
    },
    listChats: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: urlListPayment,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                $("#payment_table").bootstrapTable("destroy");
                $('#payment_table').bootstrapTable({
                    columns: [
                        {field: 'nameUser', title: 'Usuario'},
                        {field: 'servicePayment', title: 'servicePayment'},
                        {field: 'nameChat', title: 'Nombre Chat'},
                        {field: 'cryptocurrencyPayment', title: 'Cryptocurrency'},

                        {field: 'typePayment', title: 'typePayment'},
                        {field: 'ammountPayment', title: 'Monto'},
                        {field: 'descriptionPayment', title: 'descriptionPayment'},
                        {field: 'referencePayment', title: 'referencePayment'},
                        {
                            field: 'validPayment',
                            title: 'Valido',
                            align: 'center',
                            formatter: function (value, row) {
                                if (value) {
                                    return '<i data-toggle="tooltip" title="Valido" class="text-primary fa fa-certificate"></i>';
                                } else {
                                    return '<i data-toggle="tooltip" title="No Valido" class="text-gray fa fa-certificate"></i>';
                                }
                            }
                        },
                        {
                            field: 'idPayment',
                            title: 'Action',
                            align: 'center',
                            formatter: function (value, row) {
//                                return '<a class="edit_payment" data-toggle="tooltip" title="Editar">' +
//                                        '<input class="edit_payment" value="' + value + '" hidden/>' +
//                                        '<i class="text-primary fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;&nbsp;' +
//                                        '<a class="valid_payment" data-toggle="tooltip" title="Validar">' +
//                                        '<input class="valid_payment" value="' + value + '" hidden/>' +
//                                        '<i class="text-success fa fa-check-square-o"></i></a>';
                                return  '<a class="valid_payment" data-toggle="tooltip" title="Validar">' +
                                        '<input class="valid_payment" value="' + value + '" hidden/>' +
                                        '<i class="text-success fa fa-check-square-o"></i></a>';
                            }
                        }
                    ],
                    data: result.data,
                    pagination: false,
                    search: false,
                    showToggle: false
                }
                );
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    validatePayment: function (payment) {
        var that = this;
        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify({idPayment: parseInt(payment)}),
            url: urlValidatetPayment,
            beforeSend: function (xhr) {},
            success: function (result) {
                alert("Pago Validado");
                that.listChats();
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
//                alert("error creando chat");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};