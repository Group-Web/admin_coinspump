var urlListCrypto = BaseUrl + 'cryptocurrency/admin/';
var urlSaveCrypto = BaseUrl + 'cryptocurrency/wallet/';
var urlStatusCrypto = BaseUrl + 'cryptocurrency/walletstatus/';

// Objetos
var ListCrypto = {
    init: function () {
        var that = this;
        // Preloader
        this.listCrypto();

        $('#crypto_table').on('click', '#statusCryptocurrency', function (e) {
            var id_crypto = $(this).closest('tr').find('.like2').val();
            that.cryptoStatus(id_crypto);
        });

        $('#crypto_table').on('click', '.like', function (e) {
            var id_crypto = $(this).children('.like2').val();
            if ($(this).find('i').hasClass('fa-pencil')) {
                $(this).find('i').removeClass('fa-pencil').addClass('fa-check-square text-success');
                $(this).closest('tr').find('input').removeClass('hidden');
            }else{
                $(this).find('i').removeClass('fa-check-square text-success').addClass('fa-pencil');
                $(this).closest('tr').find('input').addClass('hidden');
                that.editCrypto(id_crypto,$(this).closest('tr').find('input'));
            }
        });

        $('#create_button').on('click', function(event) {
            event.preventDefault();
            $('#crypto_create').fadeIn('slow');
            $('#btn-create-crypto').on('click', function(event) {
                event.preventDefault();
                that.createCrypto();
            });
        });
        $('#btn-regresar').on('click', function(event) {
            event.preventDefault();
            $('#crypto_create').fadeOut('slow');
        });

    },
    listCrypto: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: urlListCrypto,
            beforeSend: function (xhr) {
            },
            success: function (result) {

                $('#crypto_table').bootstrapTable('destroy');
                $('#crypto_table').bootstrapTable({
                    columns: [
                        {field: 'nameCryptocurrency', title: 'Name'},
                        {
                            field: 'codeCryptocurrency', 
                            title: 'Code',
                            align: 'center',
                            formatter: function (value) {
                                    return '<p>'+value+'</p>' +
                                           '<input type="text" id="codeCryptocurrency" class="form-control hidden" value="'+value+'">';
                            }
                        },
                        {
                            field: 'webCryptocurrency',
                            title: 'Web Address',
                            align: 'center',
                            formatter: function (value) {
                                    return '<p>'+value+'</p>' +
                                           '<input type="text" id="webCryptocurrency" class="form-control hidden" value="'+value+'">';
                            }
                        },
                        {   
                            field: 'imageCryptocurrency', 
                            title: 'Image',
                            align: 'center',
                            formatter: function (value) {
                                    return '<img class="url-img" src="'+value+'" style="max-width: 30px;"/>' +
                                           '<input type="text" id="imageCryptocurrency" class="form-control hidden" value="'+value+'">';
                            }
                        },
                        {
                            field: 'statusCryptocurrency', 
                            title: 'Status',
                            align: 'center',
                            formatter: function (value) {
                                    if (value) {
                                        return '<i data-toggle="tooltip" title="Activo" class="text-primary fa fa-certificate" id="statusCryptocurrency"></i>';
                                    } else {
                                        return '<i data-toggle="tooltip" title="Inactivo" class="text-gray fa fa-certificate" id="statusCryptocurrency"></i>';
                                    }
                            }
                        },
                        {
                            field: 'idCryptocurrency',
                            title: 'Editar',
                            align: 'center',
                            formatter: function (value) {
                                    return '<a class="like" ><input class="like2" value="' + parseInt(value) + '" hidden/><i class="text-primary fa fa-pencil fa-fw"></i></a>';
                            }
                        }

                    ],
                    data: result.data,
                    pagination: true,
                    search: true,
                    showToggle: false
                });
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    cryptoStatus: function(id) {
       var that = this;
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({id: parseInt(id),changeStatus:'true'}),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: urlListCrypto,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listCrypto();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    editCrypto: function(id,inputs) {
       var that = this;
       var input = []; 
       inputs.each(function(index, el) {
           input[$(this).attr('id')] = $(this).val();
       });
       console.log(input);
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({id: parseInt(id) , codeCryptocurrency:input['codeCryptocurrency'] , imageCryptocurrency:input['imageCryptocurrency'] , webCryptocurrency:input['webCryptocurrency']}),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: urlListCrypto,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listCrypto();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    createCrypto: function() {
       var that = this;
       var input = []; 
       $('#crypto_create').find('input').each(function(index, el) {
           input[$(this).attr('id')] = $(this).val();
       });
       console.log(input);
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            data: JSON.stringify({
                nameCryptocurrency:input['nameCryptocurrency'] , 
                codeCryptocurrency:input['codeCryptocurrency'] , 
                imageCryptocurrency:input['imageCryptocurrency'] , 
                webCryptocurrency:input['webCryptocurrency']
            }),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: urlListCrypto,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listCrypto();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    }
};
var CreateWallet = {
    init: function () {
        var that = this;
        GetCryptocurrency("#cryptocurrencyWallet");
        GetWalletProvider("#providerWallet");

        $("#addWalletNew").on("click", function () {
            that.saveNewWallet();
        });
    },
    saveNewWallet: function () {
        var myData = {
            nameWallet: $("#nameWallet").val(),
            descriptionWallet: $("#descriptionWallet").val(),
            cryptocurrencyWallet: $("#cryptocurrencyWallet").val(),
            providerWallet: $("#providerWallet").val(),
            uriWallet: $("#addressWallet").val()
        };

        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify(myData),
            url: urlSaveCrypto,
            beforeSend: function (xhr) {},
            success: function (result) {
                alert("Wallet Creado");
                window.location.href = "";
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};