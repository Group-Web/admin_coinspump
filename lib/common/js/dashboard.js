var UrlDashboard = BaseUrl + 'default/dashboard';

function LoadIndex() {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        type: "GET",
        headers: {"token": localStorage['coin_admin_token']},
        url: UrlDashboard,
        beforeSend: function (xhr) { },
        success: function (result) {
            // Seccion 1 - Indicadores
            IndicadorHome(
                    result.data.totalUserValid + " / " + result.data.totalUser,
                    result.data.totalUserByEmail + " / " + result.data.totalUser,
                    nFormatter(result.data.totalPaymentValidate, 2) + " / " + nFormatter(result.data.totalPayment, 2),
                    result.data.totalPayment
                    );
//
//            Seccion 2 - Graficos
            var dataU = result.data.userByCountry, country = [], user = [];
            for (var i in dataU) {
                user.push(parseInt(dataU[i].user));
                country.push(dataU[i].name_country);
            }
            ChartBarsHome('home_chart_bar', [{name: 'usuarios', data: user, color: '#9A69E5'}], country);

////            //    Seccion 3 - Palabras favoritas, top5 categorias y ultimos usuarios
//            ProductSalesHome(result.data.sale_product);
//            ChartCakeHome(result.data.total_pu, result.data.total_client);
//            LastUserHome(result.data.user_top_point);
        },
        error: function (xhr) {
            ErrorMessage(xhr, 'no');
        },
        complete: function (jqXHR, textStatus) {
            //Oculta el loader y activa el combo
            HideLoader();
        }
    });

}

/* Seccion 1 - Indicadores principales */
function IndicadorHome(user, total_phrase, approve_phrase, complain_phrase) {
    $("#home_ind_user").html(user);
    $("#home_ind_total_phrase").html(total_phrase);
    $("#home_ind_approve_phrase").html(approve_phrase);
    $("#home_ind_complain_phrase").html(complain_phrase);
}

function IndicadorHome2(user, total_phrase, approve_phrase, complain_phrase) {
    $("#home_ind_user2").html(user);
    $("#home_ind_total_phrase2").html(total_phrase);
    $("#home_ind_approve_phrase2").html(approve_phrase);
    $("#home_ind_complain_phrase2").html(complain_phrase);
}

/* Seccion 2 - Grafico de barras del home o dashboard */
function ChartBarsHome(id_html, data2, category) {
    var chart = Highcharts.chart(id_html, {
        chart: {type: 'column'},
        title: {text: ''},
        xAxis: {categories: category},
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {text: 'Total'}
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {stacking: 'normal'}
        },
        series: data2
    });
    ResizeChart(chart);
}
function ChartCakeHome(contestadas, total) {
    var por = (contestadas * 100) / total;
//    Seccion 3 Grafico 1
    var data = [{
            name: 'Pendientes',
            y: por - 100,
            color: "#EA4937",
            sliced: true,
            selected: true
        }, {
            name: 'Contestadas',
            y: por,
            color: "#008C4E"
        }];
    var chart = Highcharts.chart('home_chart_cake', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {text: ''},
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {enabled: false},
                showInLegend: true,
                depth: 45
            }
        },
        series: [{
                name: 'Planillas de evaluación',
                data: data
            }]
    });
    ResizeChart(chart);
}
function ResizeChart(chart) {
    var height = chart.height;
    var width = $(".nav-tabs-custom").width();
    chart.setSize(width, height, doAnimation = true);
}

/* Seccion 3 - Frases Favoritas, Top5 de categorias, ultimos usuarios registrados*/
/* -- Frases o palabras favoritas */
function _structureProductSale(url, id, total, nombre, category, example) {
    return ('<li class="item">' +
            '<div class="product-img"><img src="' + url + '" alt="img"></div>' +
            '<div class="product-info">' +
            '<a class="product-title url" id="' + id + '">' + nombre +
            ' (Total ' + total + ')<span class="label bg-blue pull-right">' + category + '</span></a>' +
            '<span class="product-description">' + example + '</span>' +
            '</div></li>');
}
function ProductSalesHome(data) {
    $(".products-list").empty();
    for (var i in data) {
        if (data[i].url_product == null) {
            data[i].url_product = 'lib/common/img/sin_imagen.jpg';
        }
        $(".products-list").append(_structureProductSale(data[i].url_product, data[i].id_product, data[i].total, data[i].name_product, data[i].name_category_product, "Código del producto: " + data[i].code_product));
    }
//    $(".url").on("click", function () {
//        var id = $(this).attr("id");
//        localStorage["cdice_pfa"] = parseInt(id);
//        window.location.href = 'Phrase/edit_phrase.php';
//    });
}

/* -- TOP 5 de categorias con mas palabras */
function _structureTopCategory(name, phrase, total_phrase) {
    var porcent = (phrase * 100) / total_phrase;
    return ('<div class="progress-group">' +
            '<span class="progress-text">' + name + '</span>' +
            '<span class="progress-number"><b>' + phrase + '</b>/' + total_phrase + '</span>' +
            '<div class="progress sm">' +
            '<div class="progress-bar bg-cdice-purple" style="width: ' + porcent + '%"></div>' +
            '</div></div>');
}
function TopCategoryHome(data, total) {
    $("#home_category").empty();
    for (var i in data) {
        $("#home_category").append(_structureTopCategory(data[i].name_category, data[i].total, total));
    }
}

/* -- Ultimos usuarios registrados */
function _structureLastUser(point, name, local_client) {
    return('<li><div class="bg-cdice-blue div-home-c"><span>' + point + '</span></div>' +
            '<a class="users-list-name"><i class="fa fa-user "></i> ' + name + '</a>' +
            '<span class="users-list-date">' + local_client + '</span>' +
            '</li>');
}
function LastUserHome(data) {
    $(".users-list").empty();
    for (var i in data) {
        $(".users-list").append(_structureLastUser(nFormatter(data[i].points, 2), data[i].name_client + " " + data[i].lastname_client, data[i].local_client));
    }
}