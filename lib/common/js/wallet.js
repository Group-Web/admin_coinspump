var urlListWallet = BaseUrl + 'cryptocurrency/wallet/admin/';
var urlSaveWallet = BaseUrl + 'cryptocurrency/wallet/';
var urlStatusWallet = BaseUrl + 'cryptocurrency/walletstatus/';

// Objetos
var ListWallet = {
    init: function () {
        var that = this;
        // Preloader
        this.listWallet();
        $('#wallet_table').on('click', '.like', function (e) {
            var id_wallet = $(this).children('.like2').val();
            $("#id_walleid_wallett").val(id_wallet);
            that.walletStatus(id_wallet);
        });
    },
    listWallet: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: urlListWallet,
            beforeSend: function (xhr) {
            },
            success: function (result) {

                $("#wallet_table").bootstrapTable("destroy");
                $('#wallet_table').bootstrapTable({
                    columns: [
                        {field: 'nameProvider', title: 'Provider'},
                        {field: 'codeCryptocurrency', title: 'Code'},
                        {field: 'nameCryptocurrency', title: 'Cryptocurrency'},
                        {field: 'uriWallet', title: 'Address'},
                        {field: 'statusCryptocurrency', title: 'Status'},
                        {
                            field: 'idCryptocurrency',
                            title: 'Editar',
                            align: 'center',
                            formatter: function (value) {
                                    return '<a class="like" ><input class="like2" value="' + parseInt(value) + '" hidden/><i class="text-primary fa fa-pencil fa-fw"></i></a>';
                            }
                        }

                    ],
                    data: result.data,
                    pagination: false,
                    search: true,
                    showToggle: false
                });
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    walletStatus: function(id) {
       var that = this;
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({id: parseInt(id)}),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: urlStatusWallet,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listWallet();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    }
};
var CreateWallet = {
    init: function () {
        var that = this;
        GetCryptocurrency("#cryptocurrencyWallet");
        GetWalletProvider("#providerWallet");
        $('#create_wallet').on('click', function(event) {
            event.preventDefault();
            $('#wallet_create').fadeIn('slow');
        });
        $('#wallet_return').on('click', function(event) {
            event.preventDefault();
            $('#wallet_create').fadeOut('slow');
        });
        $("#addWalletNew").on("click", function () {
            that.saveNewWallet();
        });
    },
    saveNewWallet: function () {
        var myData = {
            nameWallet: $("#nameWallet").val(),
            descriptionWallet: $("#descriptionWallet").val(),
            cryptocurrencyWallet: $("#cryptocurrencyWallet").val(),
            providerWallet: $("#providerWallet").val(),
            uriWallet: $("#addressWallet").val()
        };

        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify(myData),
            url: urlSaveWallet,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                ListWallet.listWallet();
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    }
};