var UrlService = BaseUrl + 'service/';

// Objetos
var ListService = {
    init: function () {
        var that = this;
        var table;
        // Preloader
        this.listService();

        $('#create_button').on('click', function(event) {
            event.preventDefault();
            var that = this;
            $('#service_create').fadeOut('slow');
            $('#nameServicePayment').val('').removeAttr('disabled');
            $('#descriptionServicePayment').val('').removeAttr('disabled');
            delete sessionStorage['priceService'];
            $("#priceServiceTable").bootstrapTable("destroy");
            $('#addServiceNew').removeAttr('edit');
            $('#service_create').fadeIn('slow');

            $('#service_create').fadeIn('slow');
        });
        $('#service_return').on('click', function(event) {
            event.preventDefault();
            $('#service_create').fadeOut('slow');
        });

        $('#service_table').on('click', '.like', function (e) {
            var id_service = $(this).children('.like2').val();
            that.detailService($(this).attr('item'));
        });
    },
    listService: function () {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: UrlService,
            beforeSend: function (xhr) {
            },
            success: function (result) {

                $("#service_table").bootstrapTable("destroy");
                $('#service_table').bootstrapTable({
                    columns: [
                        {
                            field: 'nameServicePayment', 
                            title: 'Service',
                            align: 'center',
                            formatter: function (value) {
                                return '<p>'+value+'</p>';
                            }
                        },
                        {
                            field: 'descriptionServicePayment', 
                            title: 'Description',
                            align: 'center',
                            formatter: function (value) {
                                return '<p>'+value+'</p>';
                            }
                        },
                        {
                            field: 'priceServicePayment', 
                            title: 'Cryptocurrencies',
                            align: 'center',
                            formatter: function (value) {
                                var arr = [];
                                $.each(value, function( index, value ) {
                                    arr.push(value.codeCryptocurrency);
                                });
                                return '<p>'+arr.join()+'</p>';
                            }
                        },
                        /*{
                            field: 'statusServicePayment', 
                            title: 'Status',
                            align: 'center',
                            formatter: function (value) {
                                if (value) {
                                    return '<i data-toggle="tooltip" title="Activo" class="text-primary fa fa-certificate" id="statusServicePayment"></i>';
                                } else {
                                    return '<i data-toggle="tooltip" title="Inactivo" class="text-gray fa fa-certificate" id="statusServicePayment"></i>';
                                }
                            }
                        },*/
                        {
                            field: 'idServicePayment',
                            title: 'Editar',
                            align: 'center',
                            formatter: function (value, item) {
                                return "<a class='like' item='"+JSON.stringify(item)+"'' ><input id'idServicePaymen' class='like2' value='" + parseInt(value) + "' hidden/><i class='text-primary fa fa-pencil fa-fw'></i></a>";
                            }
                        }

                    ],
                    data: result.data,
                    pagination: false,
                    search: true,
                    showToggle: false
                });
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    serviceStatus: function(id) {
       var that = this;
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({id: parseInt(id),changeStatus:'true'}),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: UrlService,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listService();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    detailService: function(item) {
       var that = this;
       item = jQuery.parseJSON(item);
       console.log(item);
       $('#idServicePayment').val(item.idServicePayment);
       $('#nameServicePayment').val(item.nameServicePayment).attr('disabled','disabled');
       $('#descriptionServicePayment').val(item.descriptionServicePayment).attr('disabled','disabled');
       sessionStorage['priceService'] = JSON.stringify({data: item.priceServicePayment});
       CreateService.paintPayment();
       $('#addServiceNew').attr('edit','edit');
       $('#service_create').fadeIn('slow');
    },
    editService: function(id,inputs) {
       var that = this;
       var input = []; 
       inputs.each(function(index, el) {
           input[$(this).attr('id')] = $(this).val();
       });
       console.log(input);
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({
                id: parseInt(id), 
                nameServicePayment:input['nameServicePayment'], 
                uriServicePayment:input['uriServicePayment'], 
                descriptionServicePayment:input['descriptionServicePayment']
            }),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: UrlService,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listService();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
};
var CreateService = {
    init: function () {
        var that = this;
        sessionStorage.removeItem('priceService');

        $("#add_service_payment_price").on("click", function () {
            that.addModalPayment();
        });
        $('#priceServiceTable').on('click', '.delete', function (e) {
            that.deletePayment($(this).children('.delete').val());
        });

        $("#addServiceNew").on("click", function () {
            if ($(this).attr('edit')) {
                that.editService();
            }else{
                that.saveNewService();
            }
        });
    },
    saveNewService: function () {
        var myData = {
            nameServicePayment: $("#nameServicePayment").val(),
            descriptionServicePayment: $("#descriptionServicePayment").val(),
        };

        if (sessionStorage['priceService'] && parseInt(myData["typeChat"]) != 1) {
            var price = JSON.parse(sessionStorage['priceService']);
            myData['priceServicePayment'] = price.data;
            console.log(myData);
        }

        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify(myData),
            url: UrlService,
            beforeSend: function (xhr) {},
            success: function (result) {
                location.reload();
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    editService: function () {
        var myData = {
            idServicePayment: $("#idServicePayment").val(),
            nameServicePayment: $("#nameServicePayment").val(),
            descriptionServicePayment: $("#descriptionServicePayment").val(),
        };

        if (sessionStorage['priceService'] && parseInt(myData["typeChat"]) != 1) {
            var price = JSON.parse(sessionStorage['priceService']);
            myData['priceServicePayment'] = price.data;
        }
        console.log(myData);
        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify(myData),
            url: UrlService,
            beforeSend: function (xhr) {},
            success: function (result) {
                location.reload();
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    addModalPayment: function () {
        var that = this;
        GetTypePayment("#cryptocurrency2");
        swal({
            title: "<h4>¡Agrega un nuevo metodo de pago!</h4>",
            text: this.structureModalPrice(),
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Guardar",
            cancelButtonText: "Cancelar",
            cancelButtonClass: "btn-default",
            allowOutsideClick: true,
            html: true,
            closeOnConfirm: true
        }, function () {
            that.addPayment($("#valuePriceServicePayment").val(), $("#descriptionPriceServicePayment").val(), $("#cryptocurrency2 option:selected").val(), $("#cryptocurrency2 option:selected").text());
            that.paintPayment();
        });
    },
    addPayment: function (valuePriceServicePayment, descriptionPriceServicePayment, cryptocurrency, nameCryptocurrency) {
        if (sessionStorage['priceService']) {
            var array = JSON.parse(sessionStorage['priceService']);
            var data = array.data;
            var msj = "";
            for (var i in data) {
                if (cryptocurrency == data[i].cryptocurrency) {
                    msj = '<small>- Ya existe un monto con esta criptomoneda</small>';
                }
                if (msj != '') {
                    $(".rs").html(msj);
                    return false;
                }
            }
            array.data.push({
                valuePriceServicePayment: valuePriceServicePayment,
                descriptionPriceServicePayment: descriptionPriceServicePayment,
                cryptocurrency: cryptocurrency,
                nameCryptocurrency: nameCryptocurrency
            });
            sessionStorage['priceService'] = JSON.stringify({data: array.data});
        } else {
            sessionStorage['priceService'] = JSON.stringify({data: [{valuePriceServicePayment: valuePriceServicePayment, descriptionPriceServicePayment: descriptionPriceServicePayment, cryptocurrency: cryptocurrency, nameCryptocurrency: nameCryptocurrency}]});
        }
    },
    paintPayment: function () {
        if (sessionStorage['priceService']) {
            var data = JSON.parse(sessionStorage['priceService']).data;
            $("#priceServiceTable").show();
            $("#priceServiceTableLegend").hide();
            $("#priceServiceTable").bootstrapTable("destroy");
            $('#priceServiceTable').bootstrapTable({
                columns: [
                    {field: 'nameCryptocurrency', title: 'cryptocurrency'},
                    {field: 'valuePriceServicePayment', title: 'Monto'},
                    {field: 'descriptionPriceServicePayment', title: 'Descripcion'},
                    {
                        field: 'cryptocurrency',
                        title: 'Action',
                        align: 'center',
                        formatter: function (value, row) {
                            return '<a class="delete" data-toggle="tooltip" title="Delete">' +
                                    '<input class="delete" value="' + value + '" hidden/>' +
                                    '<i class="text-danger fa fa-window-close"></i></a>';
                        }
                    }
                ],
                data: data,
                pagination: true,
                search: false,
                showToggle: false
            });
        } else {
            $("#priceServiceTable").bootstrapTable("destroy");
            $("#priceServiceTable").hide();
            $("#priceServiceTableLegend").show();
            $("#priceServiceTableLegend").html("<p>Para agregar un precio al chat presione el boton +, de lo contrario sera tomado como un chat gratuito</p>");
        }
    },
    structureModalPrice: function () {
        return '</br><div class="col-md-12 input-group">' +
                '<input placeholder="0.00" type="text" class="form-control" id="valuePriceServicePayment">' +
                '</div></br><div class="col-md-12 input-group">' +
                '<input placeholder="Description" type="text" class="form-control" id="descriptionPriceServicePayment">' +
                '</div></br><div class="col-md-12 input-group">' +
                '<select class="form-control input-new-c cryptocurrency" id="cryptocurrency2" ></select>' +
                '</div></br>';
    },
    deletePayment: function (cryptocurrency) {
        if (sessionStorage['priceService']) {
            var data = JSON.parse(sessionStorage['priceService']).data;
            for (var i in data) {
                if (cryptocurrency == data[i].cryptocurrency) {
                    data.splice(i, 1);
                }
            }
            if (data != '') {
                sessionStorage['priceService'] = JSON.stringify({data: data});
            } else {
                sessionStorage.removeItem('priceService');
            }
            this.paintPayment();
        }
    },
};