var urlListUser = BaseUrl + 'user/';
var urlUserById = BaseUrl + 'user/byid/';
var UrlServiceCountry = BaseUrl + 'default/countries';
var UrlServiceGender = BaseUrl + 'default/genders';
var UrlServiceRole = BaseUrl + 'default/roles';
var UrlServiceRole = BaseUrl + 'default/roles';

var ListUser = {
	init: function() {
		var that = this;
		// Preloader
        sessionStorage.removeItem('priceChat');
        localStorage.removeItem('idChat');
        
        $('#user_table').on('click', '.like', function (e) {
	        var id_user = $(this).children('.like2').val();
	        $("#id_user").val(id_user);
	        that.userDetail(id_user);
	    });

        $('#user_detail').on('click', '#btn-update-user', function () {
		    that.updateProfileUser();
		});
        this.listUser();

	},
	listUser: function() {
		$.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: urlListUser,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                $("#user_table").bootstrapTable("destroy");
                $('#user_table').bootstrapTable({
                    columns: [
                        {field: 'loginUser', title: 'Username'},
                        {field: 'nameUser', title: 'Name'},
                        {field: 'lastnameUser', title: 'Lastname'},
                        {field: 'emailUser', title: 'Email'},
                        {field: 'nameCountryUser', title: 'Country'},
                        {
                            field: 'idStatusUser',
                            title: 'Status',
                            align: 'center',
                            formatter: function (value, row) {
                                switch (value) {
                                    case true:
                                    	return '<span data-toggle="tooltip" title="Activo" class="text-success fa fa-check-circle"></span>';
                                    	break;
                                    default:
                                    	return '<span data-toggle="tooltip" title="Inactivo" class="text-danger fa fa-times-circle"></span>';
                                        break;
                                }
                            }
                        },
                        {
	                        field: 'idUser',
	                        title: 'Editar',
	                        align: 'center',
	                        formatter: function (value) {
	                            return '<a class="like" ><input class="like2" value="' + parseInt(value) + '" hidden/><i class="text-primary fa fa-pencil fa-fw"></i></a>';
                        }
                    }
                    ],
                    data: result.data,
                    pagination: false,
                    search: false,
                    showToggle: false
                });
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
	},
	userDetail: function(id) {
		$.ajax({
	        async: true,
	        crossDomain: true,
	        contentType: "application/json",
	        dataType: "json",
	        type: "POST",
	        data: JSON.stringify({id: parseInt(id)}),
	        headers: {"Authorization": localStorage['coin_admin_token']},
	        url: urlUserById,
	        beforeSend: function (xhr) {
	            ShowLoader();
	        },
	        success: function (result) {
	            //localStorage['coin_admin_token'] = result.token;
	            $("#idUser").val(result.data.idUser);
	            $("#loginUser").val(result.data.loginUser);
	            $("#nameUser").val(result.data.nameUser);
	            $("#lastnameUser").val(result.data.lastnameUser);
	            $("#emailUser").val(result.data.emailUser);
	            $("#descriptionUser").val(result.data.descriptionUser);
	            $("#birthdayUser").datepicker({
	                format: "yyyy-mm-dd",
	                changeYear: true,
	                language: 'es'
	            });
	            $('#birthdayUser').datepicker("setDate", result.data.birthdayUser);
	            GetCountries("select#countryUser", result.data.idCountryUser);
	            GetGender("select#genderUser", result.data.idGenderUser);
	            GetRole("#roleUser" , result.data.idRoleUser);
	        },
	        error: function (xhr) {
	            ErrorMessage(xhr, 'no');
	        },
	        complete: function (jqXHR, textStatus) {
	            HideLoader();
	            $('#user_general').hide('slow',function() {
	            	$('#user_detail').show('slow');
	            });
	        }
	    });
	},
	updateProfileUser: function() {
		var json = JSON.stringify({
				idUser: $("#idUser").val(),
				username: $("#loginUser").val(), 
		    	email: $("#emailUser").val(), 
		    	description: $("#descriptionUser").val(), 
		    	name: $("#nameUser").val(), 
		    	lastname: $("#lastnameUser").val(), 
		    	birthday: $("#birthdayUser").val(), 
		    	country: $("#countryUser").val(), 
		    	gender: $("#genderUser").val(), 
		    	role: $("#roleUser").val()
			});

    	$.ajax({
	        async: true,
	        crossDomain: true,
	        contentType: "application/json",
	        dataType: "json",
	        type: "PUT",
	        data: json,
	        headers: {"Authorization": localStorage['coin_admin_token']},
	        url: urlListUser,
	        beforeSend: function (xhr) {
	            ShowLoader();
	        },
	        success: function (result) {
	        	console.log(result);
	        },
	        error: function (xhr) {
	            ErrorMessage(xhr, 'no');
	        },
	        complete: function (jqXHR, textStatus) {
	            HideLoader();
	            $('#user_general').hide('slow',function() {
	            	$('#user_detail').show('slow');
	            });
	        }
	    });

	}
}
