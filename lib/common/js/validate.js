// URL Services
var ret;

// Validaciones individuales Comunes
function isEmail(email) {
    email.removeClass("is-valid is-invalid");
    email.next().children().removeClass("bto-error bto-success");
    if (!email.val().match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) {
        email.next().children().addClass("bto-error").html("¡Error en el formato del correo!");
        email.addClass("is-invalid");
        return false;
    } else {
        email.next().children().addClass("bto-success").html("¡Formato de correo valido!");
        email.addClass("is-valid");
        return true;
    }

}
function isValidCombo(combo) {
    combo.removeClass("is-valid is-invalid");
    combo.next().children().removeClass("bto-error bto-success");
    if (combo.val() < 1) {
        combo.addClass("is-invalid");
        combo.next().html("¡Debe seleccionar una opción.!");
        return false;
    }
    combo.addClass("is-valid");
    return true;
}
function isValidString(name) {
    name.removeClass("is-valid is-invalid");
    name.next().children().removeClass("bto-error bto-success");
    if (name.val() == '') {
        name.addClass("is-invalid");
        name.next().children().addClass("bto-error").html("¡Este campo no puede estar vacio!");
        return false;
    } else {
        name.next().children().addClass("bto-success").html("¡Esta campo es valido!");
        name.addClass("is-valid");
        return true;
    }

}
function isValidDate(date) {
    date.removeClass("is-valid is-invalid");
    date.next().children().removeClass("bto-error bto-success");
    if (date.val().length < 1 || date.val() == '') {
        date.addClass("is-invalid");
        date.next().children().addClass("bto-error").html("¡La fecha no puede estar vacia y debe tener un formato valido!");
        return false;
    } else {
        date.next().children().addClass("bto-success").html("¡Fecha valida!");
        date.addClass("is-valid");
        return true;
    }

}
//--- Valid Pass y confirm
function iconCorrectPassword() {
    return '<span class="icon-va"><i class="fa fa-check-circle"></i></span>';
}
function SearchOneToOne(string) {
    var out = true;
    for (var i = 0; i < string.length; i++)
        if (!string.charAt(i).match(/[a-z]|[A-Z]|\d|\$|\@|\_|\./))
            out = false;
    return out;
}
function isCorrectPassword(valor) {
    var entro = true;
    valor.removeClass("is-valid is-invalid");
    valor.next().html("Debe Contener los siguientes caracteres:</br>").addClass("padin");
    if (!SearchOneToOne(valor.val())) {
        valor.next().append('<span class="text-danger">' + iconCorrectPassword() + 'Posee caracteres invalidos</span> </br>');
        entro = false;
    } else {
        valor.next().append('<span class="text-success">' + iconCorrectPassword() + 'No posee caracteres invalidos</span> </br>');
    }

    if (valor.val().length < 6) {
        valor.next().append('<span>' + iconCorrectPassword() + 'Mas de 6 caracteres.</span> </br>');
        entro = false;
    } else {
        valor.next().append('<span class="text-success">' + iconCorrectPassword() + 'Mas de 6 caracteres.</span> </br>');
    }

    if (!valor.val().match(/[a-z]/)) {
        valor.next().append('<span>' + iconCorrectPassword() + 'Al menos una letra minuscula.</span> </br>');
        entro = false;
    } else {
        valor.next().append('<span class="text-success">' + iconCorrectPassword() + 'Al menos una letra minuscula.</span> </br>');
    }

    if (!valor.val().match(/[A-Z]/)) {
        valor.next().append('<span>' + iconCorrectPassword() + 'Al menos una letra mayuscula.</span> </br>');
        entro = false;
    } else {
        valor.next().append('<span class="text-success">' + iconCorrectPassword() + 'Al menos una letra mayuscula.</span> </br>');
    }

    if (!valor.val().match(/\d/)) {
        valor.next().append('<span>' + iconCorrectPassword() + 'Al menos un número.</span> </br>');
        entro = false;
    } else {
        valor.next().append('<span class="text-success">' + iconCorrectPassword() + 'Al menos un número.</span> </br>');
    }

    if (!valor.val().match(/\$|\@|\_|\./)) {
        valor.next().append('<span>' + iconCorrectPassword() + 'Al menos un caracter especial ($ ,@ ,. ,_)</span> </br>');
        entro = false;
    } else {
        valor.next().append('<span class="text-success">' + iconCorrectPassword() + 'Al menos un caracter especial ($ ,@ ,. ,_)</span> </br>');
    }
//    if (!valor.val().match(/$|@|#|-|_|./)) {
//        valor.addClass("is-invalid");
//        valor.next().append("<span>- Debe contener al menos un caracter especial.</span></br>");
//        entro = false;
//    }
    if (entro == true) {
        valor.addClass("is-valid");
    } else {
        valor.addClass("is-invalid");

    }
    return entro;
}
function EqualPassword(valor1, valor2) {
    //condiciones dentro de la función
    valor2.removeClass("is-invalid is-valid");
    valor2.next().children().removeClass("bto-error bto-success");
    if (valor2.val().length == 0 || valor2.val() == "") {
        valor2.addClass("is-invalid");
        valor2.next().children().addClass("bto-error").html("¡Este campo no puede estar vacio!");
        return false;
    }
    if (valor1.val() != valor2.val()) {
        valor2.addClass("is-invalid");
        valor2.next().children().addClass("bto-error").html("¡Las contraseñas deben coincidir!");
        return false;
    }
    valor2.next().children().addClass("bto-success").html("¡Contraseñas iguales!");
    valor2.addClass("is-valid");
    return true;
}

// VALIDACIONES FORMULARIOS
// Register
function ValidateRegister() {
    // Variables
    var entro = true;

    //Condiciones para activar nuevamente las no validas
    if (!isValidString($("#nameUser"))) {
        entro = false;
    }
    if (!isValidString($("#lastnameUser"))) {
        entro = false;
    }
    if (!isValidCombo($('select[id=genderUser]'))) {
        entro = false;
    }
    if (!isValidDate($("#birthdayUser"))) {
        entro = false;
    }
    if (!isValidCombo($('select[id=countryUser]'))) {
        entro = false;
    }
    if (!isValidString($("#addressUser"))) {
        entro = false;
    }
    if (!isValidString($("#loginUser"))) {
        entro = false;
    }
    if (!isEmail($("#emailUser"), true)) {
        entro = false;
    }
    if (!isValidString($("#celUser"))) {
        entro = false;
    }
    if (!isValidString($("#phoneUser"))) {
        entro = false;
    }
    if (!EqualPassword($('#passwordUser'), $('#confirmPasswordUser'))) {
        entro = false;
    }
    if (!isCorrectPassword($('#passwordUser'))) {
        entro = false;
    }

    return entro;
}
function ValidateOnKeyUpRegister() {

    $('#nameUser , #lastnameUser , #loginUser , #celUser , #phoneUser , #addressUser').on('keyup', function () {
        isValidString($(this));
    });
    $('#birthdayUser').on('change', function () {
        isValidDate($(this));
    });
    $("#countryUser, #genderUser").on("change", function () {
        isValidCombo($(this));
    });
    $('#emailUser').on('keyup', function () {
        isEmail($(this), false);
    });
    $('#confirmPasswordUser').keyup(function () {
        EqualPassword($('#passwordUser'), $('#confirmPasswordUser'));
    });
    $('#passwordUser').keyup(function () {
        isCorrectPassword($(this));
        EqualPassword($('#passwordUser'), $('#confirmPasswordUser'));
    });

}

// Login
function ValidateLogin() {
    // Variables
    var entro = true;

    //Condiciones para activar nuevamente las no validas
    if (!isValidString($("#loginUser"))) {
        entro = false;
    }
    if (!isValidString($("#passwordUser"))) {
        entro = false;
    }
    return entro;
}
function ValidateOnKeyUpLogin() {

    $('#loginUser , #passwordUser').on('keyup', function () {
        isValidString($(this));
    });

}