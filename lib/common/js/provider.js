var UrlProvider = BaseUrl + 'cryptocurrency/provider/';
var urlSaveWallet = BaseUrl + 'cryptocurrency/wallet/';
var urlStatusWallet = BaseUrl + 'cryptocurrency/walletstatus/';

// Objetos
var ListProvider = {
    init: function () {
        var that = this;
        // Preloader
        this.listProvider();

        $('#create_button').on('click', function(event) {
            event.preventDefault();
            $('#provider_create').fadeIn('slow');
            $('#btn-create-crypto').on('click', function(event) {
                event.preventDefault();
                that.createCrypto();
            });
        });
        $('#provider_return').on('click', function(event) {
            event.preventDefault();
            $('#provider_create').fadeOut('slow');
        });

        $('#provider_table').on('click', '#statusProviderPayment', function (e) {
            var id_provider = $(this).closest('tr').find('.like2').val();
            that.providerStatus(id_provider);
        });

        $('#provider_table').on('click', '.like', function (e) {
            var id_provider = $(this).children('.like2').val();
            if ($(this).find('i').hasClass('fa-pencil')) {
                $(this).find('i').removeClass('fa-pencil').addClass('fa-check-square text-success');
                $(this).closest('tr').find('input').removeClass('hidden');
            }else{
                $(this).find('i').removeClass('fa-check-square text-success').addClass('fa-pencil');
                $(this).closest('tr').find('input').addClass('hidden');
                that.editProvider(id_provider,$(this).closest('tr').find('input'));
            }
        });
    },
    listProvider: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coin_admin_token']},
            type: "GET",
            url: UrlProvider,
            beforeSend: function (xhr) {
            },
            success: function (result) {

                $("#provider_table").bootstrapTable("destroy");
                $('#provider_table').bootstrapTable({
                    columns: [
                        {
                            field: 'nameProviderPayment', 
                            title: 'Provider',
                            align: 'center',
                            formatter: function (value) {
                                return '<p>'+value+'</p>' +
                                       '<input type="text" id="nameProviderPayment" class="form-control hidden" value="'+value+'">';
                            }
                        },
                        {
                            field: 'descriptionProviderPayment', 
                            title: 'Description',
                            align: 'center',
                            formatter: function (value) {
                                return '<p>'+value+'</p>' +
                                       '<input type="text" id="descriptionProviderPayment" class="form-control hidden" value="'+value+'">';
                            }
                        },
                        {
                            field: 'uriProviderPayment', 
                            title: 'Url',
                            align: 'center',
                            formatter: function (value) {
                                if (value != null) {
                                    return '<a href="'+value+'" target="_blank" >'+value+'</a>' +
                                           '<input type="text" id="uriProviderPayment" class="form-control hidden" value="'+value+'">';
                                }else{
                                    return '<p>'+value+'</p>' +
                                           '<input type="text" id="uriProviderPayment" class="form-control hidden" value="'+value+'">';
                                }
                            }
                        },
                        /*{
                            field: 'statusProviderPayment', 
                            title: 'Status',
                            align: 'center',
                            formatter: function (value) {
                                if (value) {
                                    return '<i data-toggle="tooltip" title="Activo" class="text-primary fa fa-certificate" id="statusProviderPayment"></i>';
                                } else {
                                    return '<i data-toggle="tooltip" title="Inactivo" class="text-gray fa fa-certificate" id="statusProviderPayment"></i>';
                                }
                            }
                        },*/
                        {
                            field: 'idProviderPayment',
                            title: 'Editar',
                            align: 'center',
                            formatter: function (value) {
                                    return '<a class="like" ><input id="idProviderPayment" class="like2" value="' + parseInt(value) + '" hidden/><i class="text-primary fa fa-pencil fa-fw"></i></a>';
                            }
                        }

                    ],
                    data: result.data,
                    pagination: false,
                    search: true,
                    showToggle: false
                });
            },
            error: function (xhr) {
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    providerStatus: function(id) {
       var that = this;
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({id: parseInt(id),changeStatus:'true'}),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: UrlProvider,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listProvider();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    editProvider: function(id,inputs) {
       var that = this;
       var input = []; 
       inputs.each(function(index, el) {
           input[$(this).attr('id')] = $(this).val();
       });
       console.log(input);
       $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "PUT",
            data: JSON.stringify({
                id: parseInt(id), 
                nameProviderPayment:input['nameProviderPayment'], 
                uriProviderPayment:input['uriProviderPayment'], 
                descriptionProviderPayment:input['descriptionProviderPayment']
            }),
            headers: {"Authorization": localStorage['coin_admin_token']},
            url: UrlProvider,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                that.listProvider();
            },
            error: function (xhr) {
                ErrorMessage(xhr, '');
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
};
var CreateProvider = {
    init: function () {
        var that = this;
        $("#addProviderNew").on("click", function () {
            that.saveNewProvider();
        });
    },
    saveNewProvider: function () {
        var myData = {
            nameProviderPayment: $("#nameProviderPayment").val(),
            urlProviderPayment: $("#urlProviderPayment").val(),
            descriptionProviderPayment: $("#descriptionProviderPayment").val(),
        };

        $.ajax({
            async: false,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coin_admin_token']},
            data: JSON.stringify(myData),
            url: UrlProvider,
            beforeSend: function (xhr) {},
            success: function (result) {
                GeneratePop(result.message , "", "success" , "no");
                //alert("Provider Creado");
                window.location.href = "";
            },
            error: function (xhr) {
                ErrorMessage(xhr, "no");
                console.log(xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};