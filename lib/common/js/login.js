// URL Services
var UrlLoginService = BaseUrl + 'user/login/';

// Capturar la tecla enter
$(function () {
    $('body').keyup(function (e) {
        if (e.keyCode == 13) {
            ActionLogin(e);
        }
    });
    $('#login').click(function (e) {
        ActionLogin(e);
    });
});

// Validaciones Login
function ActionLogin(e) {
    var textfield = $("#email");
    var passfield = $("#pass");
    var msj = "";

    e.preventDefault();
    if (textfield.val() == "") {
        msj = "Debe ingresar al menos un usuario";
        GeneratePop("!Faltan algunos campos!", msj, "error", "no");
    } else if (passfield.val() == "") {
        msj = "Debe ingresar su contraseña";
        GeneratePop("!Faltan algunos campos!", msj, "error", "no");
    } else {
        login();
    }

}

// Consulta Servicio y Crea sesiones
function login() {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        type: "POST",
        url: UrlLoginService,
        data: JSON.stringify({loginUser: $("#loginUser").val(), passwordUser: $("#passwordUser").val()}),
        beforeSend: function (xhr) {
            ShowLoader();
        },
        success: function (result) {
//            $(".content").hide();
            // Asignar valores
            var decoded = jwt_decode(result.data.token);
            console.log(decoded);
            if (decoded.data.idRoleUser != 1) {
                GeneratePop("¡Usuario sin privilegios", "no puede entrar", "error", "no");
                return false;
            }
            localStorage['coin_admin_token'] = result.data.token;
            localStorage['coin_admin_login'] = decoded.data.loginUser;
            localStorage['coin_admin_role'] = decoded.data.idRoleUser;
            localStorage['coin_admin_id'] = decoded.data.idUser;

            // Mensaje de bienvenida
            $("#cargador").hide();
            swal({
                title: "<span class='text-success'> ¡Bienvenido, " + localStorage['coin_admin_login'] + "! </span>",
                text: 'Panel administrativo Coins2pump',
                imageUrl: '../lib/common/img/coins2pump-logo.png',
                imageSize: '150x80',
                imageAlt: 'Custom image',
                confirmButtonClass: "btn-success",
                confirmButtonText: "Entrar",
                closeOnConfirm: false,
                allowOutsideClick: false,
                html: true
            }, function () {
                window.location.href = "../";
            });
        },
        error: function (xhr) {
//            ErrorMessage(xhr, 'no');
        },
        complete: function () {
            HideLoader();
        }
    });
}
