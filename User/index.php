<?php
$url = "../";
$name = "User";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content" >
                        <section id="user_general" class="content" >
                            <!--<div class='div-btn2 col-md-10 col-md-push-1 col-sm-12 col-xs-12'> 
                                <button type="button" class="btn btn-success pull-right" id="add-new"> <i class="fa fa-plus-circle fa-fw"></i>  Nueva Marca</button>
                            </div>-->
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-tasks fa-fw text-success"></i> <b>Listado de Usuarios</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <table id="user_table" class="table table-striped"></table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>

                        <!-- Vista editar -->
                        <section id="user_detail" class="content" hidden="">
                            <input type="hidden" value="" id="idUser">
                            <!-- Formulario de Editar -->
                            <div class="row">
                                <div class="col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-pencil fa-fw text-success"></i> <b>User Edit</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group col-md-6">
                                                <label>Username</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="loginUser">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <input type="email" class="form-control" id="emailUser">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Name</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="nameUser">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Lastname</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="lastnameUser">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Birthdat</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="birthdayUser">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Country</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <select class="form-control" id="countryUser">
                                                      <option>Venezuela</option>
                                                      <option>Brasil</option>
                                                      <option>USA</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Gender</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <select class="form-control" id="genderUser">
                                                      <option>Venezuela</option>
                                                      <option>Brasil</option>
                                                      <option>USA</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Role</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <select class="form-control" id="roleUser">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Description</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-codepen fa-fw"></i>
                                                    </div>
                                                    <textarea type="text" class="form-control" id="descriptionUser" rows="5"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12 div-btn">
                                                <button id="btn-update-user" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Actualizar</button>
                                                <button class="btn btn-primary" id="btn-regresar"><i class="fa fa-sign-out fa-fw"></i>  Regresar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                            <!-- /Formulario de Editar -->
                        </section>
                        <!-- /Vista editar-->
                    </section>
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/tableExport.js"></script>
        <script src="../lib/bootstrap-table/extensions/multiple-sort/bootstrap-table-multiple-sort.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="../lib/common/js/user.js"></script>
        <script>
        $(document).ready(function () {
           ListUser.init();
            HideLoader();
        });
        </script>
    </body>
</html>
