<?php
$url = "../";
$name = "Payment";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="UserMenu();">
        <div class="wrapper">
            <?php menu($url, $name) ?>
            <div class="content-wrapper">
                <?php loader(); ?>
                <!--Contenido de la Web-->
                <section id="content-principal" hidden="" >
                    <section class="content" >
                        <section id="brands_general" class="content" >
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="box box-success">
                                        <div class="box-header with-border">
                                            <h2 class="box-title"><i class="fa fa-money fa-fw text-success"></i> <b>Listado de pagos</b></h2>
                                        </div>
                                        <div class="box-body">
                                            <table id="payment_table" class="table table-striped"></table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </section>
                    </section>
                </section>
            </div>
            <?php footer(); ?>
        </div>
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/tableExport.js"></script>
        <script src="../lib/bootstrap-table/extensions/multiple-sort/bootstrap-table-multiple-sort.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>
        <script src="../lib/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
        <script src="../lib/common/js/payment.js"></script>
        <script>
       $(document).ready(function () {
           ListPayment.init();
       });
        </script>
    </body>
</html>
